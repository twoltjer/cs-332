# Class: CS 332
# Prof: Victor Norman
# Team: Thomas Woltjer and Charlie Newton
# Date: 10/26/2019
import select
from socket import *
from sys import stdin, stdout
import argparse

ENCODING = 'utf-8'

parser = argparse.ArgumentParser(description="A prattle client")

# Getting machine name: https://docs.python.org/3/library/socket.html#socket.gethostname
parser.add_argument("-n", "--name", dest="name", help="name to be prepended in messages (default: machine name)",
                    default=gethostname())
parser.add_argument("-s", "--server", dest="server", default="127.0.0.1",
                    help="server hostname or IP address (default: 127.0.0.1)")
parser.add_argument("-p", "--port", dest="port", type=int, default=12345,
                    help="TCP port the server is listening on (default 12345)")
parser.add_argument("-v", "--verbose", action="store_true", dest="verbose",
                    help="turn verbose output on")
args = parser.parse_args()

server = socket()
try:  # checks to see if it can connect to the server and catches if it can't informing the user why it can't
    if args.verbose:
        print("Connecting to {} on port {}".format(args.server, args.port))
    server.connect((args.server, args.port))
except gaierror:
    print("Could not connect to server: Could not get address info for {}".format(args.server))
    exit(-1)
except ConnectionRefusedError:
    print("Could not connect to server: Connection refused")
    exit(-1)
except TimeoutError:
    print("Could not connect to server: Connection timed out")
    exit(-1)

while True:
    input_sources, _, _ = select.select([stdin, server], [], [])  # wait for a message from a source
    for source in input_sources:
        if source == server:  # Check message source: either the server or stdin
            try:  # displays the message to user
                message = source.recv(2048).decode(ENCODING)
                print(message)
            except:
                print("Error with either recv() system call, or decoding the received message")
        else:
            try:  # Send a message entered in the terminal
                message = stdin.readline()  # read a line from the console
                server.send(bytearray("{}: {}".format(args.name, message), ENCODING))
            except:
                print("Error encoding or using send() system call")
    stdout.flush()
