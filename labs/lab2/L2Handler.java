/**
 * This handler will be responsible for:
 *  - sending a given L2Frame to layer1 to be sent, and
 *  - receiving a string of bits from layer1, and creating a L2Frame from them.
 *
 *  When receiving a frame from layer 1, instances of this class will also decide if that frame should be received or
 *  dropped and if received, passing the received frame to any object at an upper layer that has requested to receive
 *  frames.
 *
 *  @author Thomas Woltjer (tw34) and Charlie Newton (ccn4)
 */
public class L2Handler implements BitListener {
    private int macAddr;
    private Layer2Listener layer2Listener;
    private BitHandler bitHandler;

    /**
     * Create a layer 2 handler, with the given host, port, and a unique layer 2 identifier
     * @param host the host to connect to
     * @param port the port to connect on
     * @param macAddr this client's unique layer 2 identifier
     */
    public L2Handler(String host, int port, int macAddr) {
        this.macAddr = macAddr;
        bitHandler = new BitHandler(host, port);
        bitHandler.setListener(this);
    }

    /**
     * Create a layer 2 handler, using the local machine and the default port
     * @param macAddr the unique layer 2 identifier for this client
     */
    public L2Handler(int macAddr)
    {
        this("localhost", LightSystem.DEFAULT_PORT, macAddr);
    }

    @Override
    public void bitsReceived(BitHandler handler, String bits) {
        L2Frame frame = new L2Frame(bits);
        layer2Listener.frameReceived(this, frame);
    }

    /**
     * Get this client's unique layer 2 identifier
     * @return a unique ID
     */
    public int getMacAddr() {
        return macAddr;
    }

    /**
     * Get a string representation of this handler's unique address
     * @return a string representation of this handler's address
     */
    @Override
    public String toString() {
        return Integer.toString(this.macAddr);
    }

    /**
     * Set the layer 2 listener for this client
     * @param layer2Listener a Layer2Listener
     */
    public void setListener(Layer2Listener layer2Listener) {
        this.layer2Listener = layer2Listener;
    }

    /**
     * Send a frame from this client over the network
     * @param frame an object with frame info
     */
    public void send(L2Frame frame) {
        String frameContents = frame.toString();
        while(true) {
            if (bitHandler.isSilent())
                break;
            BitHandler.pause(BitHandler.HALFPERIOD);
        }
        try {
            bitHandler.broadcast(frameContents);
        } catch (CollisionException e) {
            // We should never have a collision exception because we are waiting for silence
            e.printStackTrace();
        }
    }
}
