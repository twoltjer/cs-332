import java.awt.event.*;
import javax.swing.*;

/**
 * Started by Prof. Vic Norman
 * @author Thomas Woltjer (tw34) and Charlie Newton (ccn4)
 */
public class Layer2Display implements ActionListener, Layer2Listener {
    private L2Handler handler;
    private JTextField displayField;
    private JTextField addressField;
    private JTextField payloadField;

    public Layer2Display(L2Handler handler) {
	this.handler = handler;
	handler.setListener(this);

	JFrame frame = new JFrame(handler.toString());
	frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(),
						       BoxLayout.PAGE_AXIS));

	displayField = new JTextField(20);
	displayField.setEditable(false);
	frame.getContentPane().add(displayField);

	frame.getContentPane().add(new JLabel("Address:"));

	addressField = new JTextField(20);
	addressField.addActionListener(this);
	frame.getContentPane().add(addressField);

	frame.getContentPane().add(new JLabel("Payload:"));

	payloadField = new JTextField(20);
	payloadField.addActionListener(this);
	frame.getContentPane().add(payloadField);

	frame.pack();
	frame.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
	displayField.setText("Sending...");
	new Thread() {
	    public void run() {
			L2Frame frame = new L2Frame(
					handler.getMacAddr(),
					Integer.parseInt(addressField.getText()),
					0,
					payloadField.getText().length(),
					payloadField.getText()
			);
			handler.send(frame);
	    }
	}.start();
    }

	@Override
	public void frameReceived(L2Handler h, L2Frame f) {
		if (f.getDestinationAddress() == L2Frame.BCAST_ADDR || f.getDestinationAddress() == h.getMacAddr()) {
			payloadField.setText(f.getPayload());
		}
	}
}