/**
 * @author Thomas Woltjer (tw34) and Charlie Newton (ccn4)
 */
public class Test {
	public static void runTests()
	{
		LightSystem testLightSystem = new LightSystem();
		L2FrameTest.runL2FrameTests();
		L2HandlerTest.runL2HandlerTests();
		System.exit(0);
	}

	public static void main(String[] args) {
		//runTests();

		LightSystem system = new LightSystem();
		Layer2Display d1 = new Layer2Display(new L2Handler("maroon15", LightSystem.DEFAULT_PORT, 1));
		Layer2Display d2 = new Layer2Display(new L2Handler("maroon15", LightSystem.DEFAULT_PORT, 2));
		Layer2Display d3 = new Layer2Display(new L2Handler("maroon15", LightSystem.DEFAULT_PORT, 3));
	}
}