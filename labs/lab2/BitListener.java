/**
 * @author Thomas Woltjer (tw34) and Charlie Newton (ccn4)
 */
public interface BitListener {
    void bitsReceived(BitHandler handler, String bits);
}