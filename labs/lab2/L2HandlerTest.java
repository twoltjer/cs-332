/**
 * @author Thomas Woltjer (tw34) and Charlie Newton (ccn4)
 */
public class L2HandlerTest {
    public static void runL2HandlerTests() {
        testConstructors();
        testToString();
        testSend();
    }

    private static void testConstructors() {
        L2Handler subject = new L2Handler("localhost", LightSystem.DEFAULT_PORT, 2);
        assert subject.getMacAddr() == 2 : "Mac address not set correctly";

        L2Handler subject2 = new L2Handler(3);
        assert subject2.getMacAddr() == 3 : "Mac address not set correctly";
    }

    private static void testToString() {
        L2Handler subject1 = new L2Handler(4);
        assert subject1.toString().equals("4");
    }

    private static void testSend() {
        L2Handler subject = new L2Handler(5);
        L2Frame frame = new L2Frame(1, 2, 0, 4, "1001");
        subject.send(frame);
    }
}