/**
 * The L2Frame stores an instance variable for each field in a
 * layer 2 frame: the source and destination MAC addresses, vlanId, length, etc., along with
 * a payload String.
 *
 * @author Thomas Woltjer (tw34) and Charlie Newton (ccn4)
 */
public class L2Frame {
    public static final int BCAST_ADDR = 0;

    private int source;
    private int destination;
    private int vlanId;
    private int length;
    private String payload;

    /**
     * Default constructor. Initializes an empty instance with all variables set to 0 or empty string.
     */
    public L2Frame() {
        source = destination = vlanId = length = 0;
        payload = "";
    }

    /**
     * Explicit constructor
     * @param source the source address
     * @param destination the destination address
     * @param vlanId the Vlan ID
     * @param length the data length
     * @param payload the payload data
     */
    public L2Frame(int source, int destination, int vlanId, int length, String payload) {
        this.source = source;
        this.destination = destination;
        this.vlanId = vlanId;
        this.length = length;
        this.payload = payload;
    }

    /**
     * Create a layer2 frame from its bitstring representation
     * @param frameContents a bitstring
     */
    public L2Frame(String frameContents) {
        this.destination = Integer.parseInt(frameContents.substring(1, 5), 2);
        this.source = Integer.parseInt(frameContents.substring(5, 9), 2);
        this.vlanId = Integer.parseInt(frameContents.substring(11, 13), 2);
        this.length = Integer.parseInt(frameContents.substring(13, 21), 2);
        int checksum = Integer.parseInt(frameContents.substring(21, 22));
        this.payload = frameContents.substring(22);
        validateFrame(frameContents.charAt(0), payload, checksum, length);
    }

    private static void validateFrame(char frameStart, String payload, int checksum, int length)
    {
        if (frameStart != '0')
            throw new IllegalArgumentException("Frame must begin with 0");
        if (payload.length() != length)
            throw new IllegalArgumentException("Payload length mismatch");
        if (Integer.parseInt(computeErrorCheck(payload)) != checksum)
            throw new IllegalArgumentException("Checksum failed");
    }


    /**
     * Gets the source address
     * @return the source address
     */
    public int getSourceAddress()
    {
        return this.source;
    }

    /**
     * Gets the destination
     * @return the destination address
     */
    public int getDestinationAddress()
    {
        return this.destination;
    }

    /**
     * Gets the vlan ID for this frame
     * @return a vlan ID
     */
    public int getVlanId() {
        return this.vlanId;
    }

    /**
     * Get the frame's data length
     * @return the length of the data
     */
    public int getLength() {
        return this.length;
    }

    /**
     * Get the payload contents
     * @return the payload data
     */
    public String getPayload() {
        return this.payload;
    }

    /**
     * Convert a value into a bitstring
     * @param value the integer value to convert
     * @param length the maximum length of the bitstring
     * @return a bitstring with a binary representation of the given value
     * @throws IllegalArgumentException an exception if the given length is too short to contain the given value
     */
    public static String toBinary(int value, int length) throws IllegalArgumentException {
        String bitString = Integer.toString(value, 2);
        if (bitString.length() > length)
            throw new IllegalArgumentException("Not long enough to store value");
        while (bitString.length() < length)
            bitString = "0" + bitString;
        return bitString;
    }

    /**
     * Get a parity bit for a value
     * @param valueBitString a value
     * @return an error check string representing a parity bit
     */
    public static String computeErrorCheck(String valueBitString) {
        int numberOfOnes = 0;
        for (int i = 0; i < valueBitString.length(); i++)
        {
            if (valueBitString.charAt(i) == '1')
                numberOfOnes++;
        }
        return Integer.toString(numberOfOnes % 2);
    }

    /**
     * Create a bitstring for this whole frame
     * @return the frame, in bitstring form
     */
    public String toString() {
        return "0" +
                toBinary(destination, 4) +
                toBinary(source, 4) +
                toBinary(0, 2) +
                toBinary(vlanId, 2) +
                toBinary(length, 8) +
                computeErrorCheck(payload) +
                payload;
    }
}