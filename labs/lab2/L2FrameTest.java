/**
 * @author Thomas Woltjer (tw34) and Charlie Newton (ccn4)
 */
public class L2FrameTest {
    public static void runL2FrameTests() {
        testConstructors();
        testToBinary();
        testCalculateErrorCheck();
        testToString();
    }

    private static void testConstructors() {
        // Test explicit constructor
        int source_addr = 123;
        int dest_addr = 321;
        int vlanId = 4;
        int length = 33;
        String payload = "hello there";
        L2Frame testFrame1 = new L2Frame(source_addr, dest_addr, vlanId, length, payload);
        assert testFrame1.getSourceAddress() == source_addr : "Bad source address";
        assert testFrame1.getDestinationAddress() == dest_addr : "Bad destination address";
        assert testFrame1.getVlanId() == vlanId : "Bad Vlan ID";
        assert testFrame1.getLength() == length : "Bad length";
        assert testFrame1.getPayload() == payload : "Bad payload";

        // Test default constructor
        L2Frame testFrame2 = new L2Frame();
        assert testFrame2.getSourceAddress() == 0 : "Bad source address";
        assert testFrame2.getDestinationAddress() == 0 : "Bad destination address";
        assert testFrame2.getVlanId() == 0 : "Bad Vlan ID";
        assert testFrame2.getLength() == 0 : "Bad length";
        assert testFrame2.getPayload() == "" : "Bad payload";

        // Test bitstring constructor
        L2Frame testFrame3 = new L2Frame("00010000100000000010001001");
        assert testFrame3.getSourceAddress() == 1 : "Bad source address";
        assert testFrame3.getDestinationAddress() == 2 : "Bad dest address";
        assert testFrame3.getVlanId() == 0 : "Bad vlan ID";
        assert testFrame3.getPayload().equals("1001");
    }

    private static void testToBinary() {
        assert L2Frame.toBinary(5, 4).equals("0101") : "Failed toBinary";
        assert L2Frame.toBinary(15, 4).equals("1111") : "Failed toBinary";
        assert L2Frame.toBinary(0,4).equals("0000") : "Failed toBinary";
        assert L2Frame.toBinary(0, 2).equals("00");
        try
        {
            L2Frame.toBinary(15,2);
            assert false : "Exception not thrown";
        }
        catch (IllegalArgumentException e)
        {
            assert e.getMessage().equals("Not long enough to store value");
        }
    }

    private static void testCalculateErrorCheck() {
        assert L2Frame.computeErrorCheck(L2Frame.toBinary(5, 4)).equals("0");
        assert L2Frame.computeErrorCheck(L2Frame.toBinary(15, 4)).equals("0");
        assert L2Frame.computeErrorCheck(L2Frame.toBinary(14, 4)).equals("1");
    }

    private static void testToString() {
        L2Frame frame = new L2Frame(1, 2, 0, 4, "1001");
        assert frame.toString().equals("00010000100000000010001001") : "Malformed frame";
    }


}