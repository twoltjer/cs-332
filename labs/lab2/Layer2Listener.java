/**
 * @author Thomas Woltjer (tw34) and Charlie Newton (ccn4)
 */
public interface Layer2Listener {
    public void frameReceived(L2Handler h, L2Frame f);
}
