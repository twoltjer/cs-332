import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.Random;

public class Part3Sender {
    private String filename;
    private FileInputStream fileInputStream;
    private InetAddress address;
    public int bytesRead;
    private DatagramSocket socket;
    private byte[] packetData;
    private int serverPort;
    private boolean transferComplete;
    public int packetNumber;
    private int connectionId;

    final int CONNECTION_ID_BYTES = 4;
    final int CONNECTION_ID_OFFSET = 0;
    final int FILE_SIZE_BYTES = 4;
    final int FILE_SIZE_OFFSET = CONNECTION_ID_OFFSET + CONNECTION_ID_BYTES;
    final int PACKET_NUMBER_BYTES = 4;
    final int PACKET_NUMBER_OFFSET = FILE_SIZE_OFFSET + FILE_SIZE_BYTES;
    final int PAYLOAD_BYTES = 1450;
    final int PAYLOAD_OFFSET = PACKET_NUMBER_OFFSET + PACKET_NUMBER_BYTES;

    public static void main(String[] args) {
        if (args.length != 3) {
            System.err.println("Program requires three arguments: a host, a port number, and a file name to write to.");
            System.exit(1);
        }
        // Error in case argument is not a number
        int port = -1;
        try {
            port = Integer.parseInt(args[1]);
        } catch(NumberFormatException e) {
            System.err.println("Could not parse integer from port number argument. Exiting");
            System.exit(1);
        }
        // Set filename
        String filename = args[2];
        // Destination host
        String destHost = args[0];

        Part3Sender sender = new Part3Sender(destHost, port, filename);
        while (true) {
            try {
                sender.readFileData();
                sender.generateHeader();
                sender.sendFileData();
                sender.receiveAck();
                if (sender.bytesRead < sender.PAYLOAD_BYTES)
                    break;
                sender.packetNumber++;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            sender.close();
        } catch (IOException e) {
            System.err.println("Error stopping");
        }
    }

    public Part3Sender(String destAddress, int port, String filename) {
        serverPort = port;
        transferComplete = false;
        this.filename = filename;
        this.packetNumber = 0;
        Random rand = new Random();
        this.connectionId = rand.nextInt();

        try {
            // Start file reading and create socket
            fileInputStream = new FileInputStream(filename);
            address = InetAddress.getByName(destAddress);
            socket = new DatagramSocket();
        } catch (IOException e) {
            System.out.println("IO exception!");
            e.printStackTrace();
        }
    }

    public void readFileData() throws IOException {
        packetData = new byte[PAYLOAD_OFFSET + PAYLOAD_BYTES];
        bytesRead = fileInputStream.read(packetData, PAYLOAD_OFFSET, PAYLOAD_BYTES);
    }

    public void sendFileData() throws IOException {
        // Send data in chunks of PACKET_SIZE
        System.out.println(String.format("Read %d bytes", bytesRead));
        DatagramPacket packet = new DatagramPacket(packetData, PAYLOAD_OFFSET + bytesRead, address, serverPort);
        socket.send(packet);
        System.out.println("Sent packet");

        if (bytesRead != packetData.length)
            System.out.println("Packet length is short, so exiting");
    }

    public boolean receiveAck() throws IOException {
        // Receive ACK
        byte[] ackBytes = new byte["ACK".getBytes().length];
        DatagramPacket ackPacket = new DatagramPacket(ackBytes, ackBytes.length);
        socket.receive(ackPacket);
        if (byteArraysAreEqual(ackPacket.getData(), "ACK".getBytes())) {
            System.out.println("Received ACK");
            System.out.println("====================");
            return true;
        }
        return false;
    }

    public void close() throws IOException {
        socket.close();
        fileInputStream.close();
    }

    public static boolean byteArraysAreEqual(byte[] a, byte[] b) {
        if (a.length != b.length)
            return false;
        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i])
                return false;
        }
        return true;
    }

    public void generateHeader() {
        byte[] header = new byte[CONNECTION_ID_BYTES + FILE_SIZE_BYTES + PACKET_NUMBER_BYTES];
        byte[] connectionId = getConnectionId();
        System.out.println(String.format("Connection ID: %d", this.connectionId));
        byte[] fileSize = getFileSize(filename);
        byte[] packetNumber = getPacketNumber();
        System.out.println(String.format("Packet #: %d", this.packetNumber));
        for (int i = 0; i < CONNECTION_ID_BYTES; i++) {
            header[i + CONNECTION_ID_OFFSET] = connectionId[i];
        }
        for (int i = 0; i < FILE_SIZE_BYTES; i++) {
            header[i + FILE_SIZE_OFFSET] = fileSize[i];
        }
        for (int i = 0; i < PACKET_NUMBER_BYTES; i++) {
            header[i + PACKET_NUMBER_OFFSET] = packetNumber[i];
        }
        for (int i = 0; i < header.length; i++)
            packetData[i] = header[i];
    }

    private byte[] getPacketNumber() {
        return intToBytes(this.packetNumber);
    }

    private byte[] getConnectionId() {
        return intToBytes(this.connectionId);
    }

    public byte[] getFileSize(String filename) {
        File f = new File(filename);
        return intToBytes((int) f.length());
    }

    public byte[] intToBytes(int value) {
        ByteBuffer buf = ByteBuffer.allocate(Integer.BYTES);
        buf.putInt(value);
        return buf.array();
    }

    private static void arrayPrint(byte[] anByteArray) {
        for (byte Number: anByteArray) {
            System.out.format("%d \t", Number);
        }
    }
}
