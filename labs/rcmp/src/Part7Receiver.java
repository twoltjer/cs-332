import java.io.FileOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;

/**
 * Receives RCMP packets
 * By Thomas Woltjer and Charles Newton
 */
public class Part7Receiver {
    private final int CONNECTION_ID_BYTES = 4;
    private final int CONNECTION_ID_OFFSET = 0;
    private final int FILE_SIZE_BYTES = 4;
    private final int FILE_SIZE_OFFSET = CONNECTION_ID_OFFSET + CONNECTION_ID_BYTES;
    private final int PACKET_NUMBER_BYTES = 4;
    private final int PACKET_NUMBER_OFFSET = FILE_SIZE_OFFSET + FILE_SIZE_BYTES;
    private final int PLEASE_ACK_BYTES = 1;
    private final int PLEASE_ACK_OFFSET = PACKET_NUMBER_OFFSET + PACKET_NUMBER_BYTES;
    private final int PAYLOAD_BYTES = 1450;
    private final int PAYLOAD_OFFSET = PLEASE_ACK_OFFSET + PLEASE_ACK_BYTES;

    private int currentPacketNumber;
    private int fileSize;
    private int connectionId;
    private boolean fileComplete;
    private FileOutputStream fileOutputStream;
    private byte[] packetData;
    private DatagramSocket receiveSocket;
    private InetAddress clientAddress;
    private int clientPort;
    private int errorCount = 0;
    private int totalBytesWritten;
    private int requestedAckNumber;
    private boolean skipPacket;
    private boolean sendAckReply;

    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("Program requires two arguments: a port number, and a file name to write to.");
            System.exit(1);
        }
        // Error in case argument is not a number
        int port = -1;
        try {
            port = Integer.parseInt(args[0]);
        } catch(NumberFormatException e) {
            System.err.println("Could not parse integer from port number argument. Exiting");
            System.exit(1);
        }
        // Set filename
        String filename = args[1];

        Part7Receiver receiver = new Part7Receiver(port, filename);
        while(!receiver.isFileComplete()) {
            try {
                receiver.receiveData();
                if (receiver.sendAckReply) {
                    System.out.println(String.format("Requested ack: %d", receiver.requestedAckNumber));
                    receiver.sendAck();
                }
                if (!receiver.skipPacket)
                    receiver.currentPacketNumber++;

                // Reset error count
                receiver.errorCount = 0;
                System.out.println("===============");
            } catch (IOException e) {
                if (receiver.errorCount < 3) {
                    // Don't send ack, because of a failure to receive
                    // Increment error count. If three errors on the same packet, give up.
                    receiver.errorCount++;
                } else {
                    // Give up
                    System.err.println("Too many errors while trying to receive a packet");
                    System.exit(1);
                }
            }
        }
        receiver.close();
        System.out.println("File transfer completed successfully");
    }

    private boolean isFileComplete() {
        return this.fileComplete;
    }

    private Part7Receiver(int port, String filename) {
        this.fileComplete = false;
        this.connectionId = 0;
        this.fileSize = 0;
        this.totalBytesWritten = 0;
        this.currentPacketNumber = 0;
        try {
            // Create file output
            fileOutputStream = new FileOutputStream(filename);

            // Start listening for packets
            packetData = new byte[PAYLOAD_BYTES + PAYLOAD_OFFSET];
            System.out.println("Waiting for a client to connect");
            receiveSocket = new DatagramSocket(port);
        } catch (SocketException e) {
            System.out.println("Socket exception!");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IO exception!");
            e.printStackTrace();
        }
    }

    private void receiveData() throws IOException {
        // Receive a packet, and write it to the output file
        skipPacket = false;
        DatagramPacket packet = new DatagramPacket(packetData, PAYLOAD_BYTES + PAYLOAD_OFFSET);
        receiveSocket.receive(packet);
        System.out.println("Received packet");
        clientAddress = packet.getAddress();
        clientPort = packet.getPort();
        int connectionId = getConnectionId(packet.getData());
        //System.out.println(String.format("Connection ID: %d", connectionId));
        int packetNumber = getPacketNumber(packet.getData());
        System.out.println(String.format("Recv Packet #: %d", packetNumber));
        if (this.fileSize == 0)
            this.fileSize = getFileSize(packet.getData());
        System.out.println(String.format("File size: %d", fileSize));
        byte pleaseAck = packet.getData()[PLEASE_ACK_OFFSET];
        boolean ackRequested = (pleaseAck == (byte) 1);
        System.out.println(String.format("Please ack: %d", pleaseAck));

        // Skip packet if it's old, or ahead of time, or from an incorrect connection ID
        System.out.println(String.format("Awaiting data for packet number: %d", this.currentPacketNumber));
        this.skipPacket = checkConnectionId(connectionId) && (packetNumber != this.currentPacketNumber);
        if (!skipPacket) {
            // Append packet to file
            System.out.println("Appending packet data");
            int payloadLength = packet.getLength() - PAYLOAD_OFFSET;
            fileOutputStream.write(packet.getData(), PAYLOAD_OFFSET, payloadLength);
            this.totalBytesWritten += payloadLength;
        } else {
            System.out.println("Skipping packet");
        }

        // Send a reply, even if it may be old
        this.sendAckReply = checkConnectionId(connectionId) && ackRequested && packetNumber <= this.currentPacketNumber;
        if (this.sendAckReply) {
            this.requestedAckNumber = packetNumber;
        }

        System.out.println(String.format("Total bytes written: %d", this.totalBytesWritten));
        this.fileComplete = (this.totalBytesWritten >= this.fileSize);
    }

    private boolean checkConnectionId(int connectionId) {
        if (this.connectionId == 0) {
            this.connectionId = connectionId;
        }
        return (this.connectionId == connectionId);
    }

    private int getFileSize(byte[] payload)
    {
        byte[] fileSizeBytes = new byte[FILE_SIZE_BYTES];
        for (int i = 0; i < FILE_SIZE_BYTES; i++) {
            fileSizeBytes[i] = payload[FILE_SIZE_OFFSET + i];
        }
        return bytesToInt(fileSizeBytes);
    }

    private int getPacketNumber(byte[] payload)
    {
        byte[] packetNumberBytes = new byte[PACKET_NUMBER_BYTES];
        for (int i = 0; i < PACKET_NUMBER_BYTES; i++) {
            packetNumberBytes[i] = payload[PACKET_NUMBER_OFFSET + i];
        }
        return bytesToInt(packetNumberBytes);
    }

    private int getConnectionId(byte[] payload)
    {
        byte[] connectionIdBytes = new byte[CONNECTION_ID_BYTES];
        for (int i = 0; i < CONNECTION_ID_BYTES; i++) {
            connectionIdBytes[i] = payload[CONNECTION_ID_OFFSET + i];
        }
        return bytesToInt(connectionIdBytes);
    }

    private int bytesToInt(byte[] valueBytes) {
        ByteBuffer buf = ByteBuffer.allocate(Integer.BYTES);
        buf.put(valueBytes);
        return buf.getInt(0);
    }

    private byte[] intToBytes(int value) {
        ByteBuffer buf = ByteBuffer.allocate(Integer.BYTES);
        buf.putInt(value);
        return buf.array();
    }

    private void sendAck() throws IOException {
        // Create ACK packet
        byte[] ackData = new byte[CONNECTION_ID_BYTES + PACKET_NUMBER_BYTES];
        byte[] connectionIdBytes = intToBytes(this.connectionId);
        byte[] packetNumberBytes = intToBytes(this.requestedAckNumber);
        for (int i = 0; i < CONNECTION_ID_BYTES; i++) {
            ackData[i] = connectionIdBytes[i];
        }
        for (int i = 0; i < PACKET_NUMBER_BYTES; i++) {
            ackData[i + CONNECTION_ID_BYTES] = packetNumberBytes[i];
        }

        System.out.println("Sending ack");
        DatagramPacket packet = new DatagramPacket(ackData, 0, ackData.length, clientAddress, clientPort);
        receiveSocket.send(packet);
    }

    public void close() {
        // Clean up
        try {
            fileOutputStream.close();
        } catch (IOException e) {
            System.err.println("Could not close file. Stack trace follows:");
            e.printStackTrace();
        }
        receiveSocket.close();
    }

    private static void arrayPrint(byte[] anByteArray) {
        for (byte Number: anByteArray) {
            System.out.format("%d \t", Number);
        }
    }
}
