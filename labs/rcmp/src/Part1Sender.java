import java.io.*;
import java.net.*;

public class Part1Sender {
    final int PACKET_SIZE = 1450;
    public static void main(String[] args) {
        if (args.length != 3) {
            System.err.println("Program requires three arguments: a host, a port number, and a file name to write to.");
            System.exit(1);
        }
        // Error in case argument is not a number
        int port = -1;
        try {
            port = Integer.parseInt(args[1]);
        } catch(NumberFormatException e) {
            System.err.println("Could not parse integer from port number argument. Exiting");
            System.exit(1);
        }
        // Set filename
        String filename = args[2];
        // Destination host
        String destHost = args[0];

        Part1Sender sender = new Part1Sender(destHost, port, filename);
    }

    public Part1Sender(String destAddress, int port, String filename) {
        try {
            // Start file reading and create socket
            FileInputStream fileInputStream = new FileInputStream(filename);
            InetAddress address = InetAddress.getByName(destAddress);
            DatagramSocket socket = new DatagramSocket();

            while(true) {
                // Send data in chunks of PACKET_SIZE
                byte[] packetData = new byte[PACKET_SIZE];
                int bytesRead = fileInputStream.read(packetData, 0, PACKET_SIZE);
                System.out.println(String.format("Read %d bytes", bytesRead));
                DatagramPacket packet = new DatagramPacket(packetData, bytesRead, address, port);
                socket.send(packet);
                System.out.println("Sent packet");

                // Clean up and exit if the end of the file is reached (indicated by read() returning a smaller value)
                if (bytesRead != packetData.length) {
                    System.out.println("Packet length is short, so exiting");
                    socket.close();
                    fileInputStream.close();
                    break;
                }
            }
        } catch (IOException e) {
            System.out.println("IO exception!");
            e.printStackTrace();
        }
    }
}
