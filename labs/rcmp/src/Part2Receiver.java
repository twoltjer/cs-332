import java.io.FileOutputStream;
import java.io.IOException;
import java.net.*;

public class Part2Receiver {
    final int PACKET_SIZE = 1450;
    private boolean fileComplete;
    private FileOutputStream fileOutputStream;
    private byte[] packetData;
    private DatagramSocket receiveSocket;
    private InetAddress clientAddress;
    private int clientPort;
    public int errorCount = 0;

    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("Program requires two arguments: a port number, and a file name to write to.");
            System.exit(1);
        }
        // Error in case argument is not a number
        int port = -1;
        try {
            port = Integer.parseInt(args[0]);
        } catch(NumberFormatException e) {
            System.err.println("Could not parse integer from port number argument. Exiting");
            System.exit(1);
        }
        // Set filename
        String filename = args[1];

        Part2Receiver receiver = new Part2Receiver(port, filename);
        while(!receiver.isFileComplete()) {
            try {
                receiver.receiveData();
                receiver.sendAck();
                // Reset error count
                receiver.errorCount = 0;
            } catch (IOException e) {
                if (receiver.errorCount < 3) {
                    // Don't send ack, because of a failure to receive
                    // Increment error count. If three errors on the same packet, give up.
                    receiver.errorCount++;
                    continue;
                } else {
                    // Give up
                    System.err.println("Too many errors while trying to receive a packet");
                    System.exit(1);
                }
            }
        }
        receiver.close();
    }

    private boolean isFileComplete() {
        return this.fileComplete;
    }

    public Part2Receiver(int port, String filename) {
        this.fileComplete = false;
        try {
            // Create file output
            fileOutputStream = new FileOutputStream(filename);

            // Start listening for packets
            packetData = new byte[PACKET_SIZE];
            receiveSocket = new DatagramSocket(port);
        } catch (SocketException e) {
            System.out.println("Socket exception!");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IO exception!");
            e.printStackTrace();
        }
    }

    public void receiveData() throws IOException {
        // Receive a packet, and write it to the output file
        DatagramPacket packet = new DatagramPacket(packetData, PACKET_SIZE);
        receiveSocket.receive(packet);
        fileOutputStream.write(packet.getData(), 0, packet.getLength());
        clientAddress = packet.getAddress();
        clientPort = packet.getPort();
        System.out.println(String.format("Packet data length: %d bytes", packet.getLength()));
        if (packet.getLength() != PACKET_SIZE)
            fileComplete = true;
    }

    public void sendAck() throws IOException {
        // Create ACK packet
        byte[] ackData = "ACK".getBytes();
        System.out.println("Sending ack");
        DatagramPacket packet = new DatagramPacket(ackData, 0, ackData.length, clientAddress, clientPort);
        arrayPrint(packet.getData());
        receiveSocket.send(packet);
    }

    public void close() {
        // Clean up
        try {
            fileOutputStream.close();
        } catch (IOException e) {
            System.err.println("Could not close file. Stack trace follows:");
            e.printStackTrace();
        }
        receiveSocket.close();
    }

    private static void arrayPrint(byte[] anByteArray) {
        for (byte Number: anByteArray) {
            System.out.format("%d \t", Number);
        }
    }
}
