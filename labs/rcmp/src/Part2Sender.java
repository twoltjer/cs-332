import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Part2Sender {
    private FileInputStream fileInputStream;
    private InetAddress address;
    public int bytesRead;
    private DatagramSocket socket;
    private byte[] packetData;
    private int serverPort;
    private boolean transferComplete;

    final int PACKET_SIZE = 1450;
    public static void main(String[] args) {
        if (args.length != 3) {
            System.err.println("Program requires three arguments: a host, a port number, and a file name to write to.");
            System.exit(1);
        }
        // Error in case argument is not a number
        int port = -1;
        try {
            port = Integer.parseInt(args[1]);
        } catch(NumberFormatException e) {
            System.err.println("Could not parse integer from port number argument. Exiting");
            System.exit(1);
        }
        // Set filename
        String filename = args[2];
        // Destination host
        String destHost = args[0];

        Part2Sender sender = new Part2Sender(destHost, port, filename);
        while (true) {
            try {
                sender.readFileData();
                sender.sendFileData();
                sender.receiveAck();
                if (sender.bytesRead < sender.PACKET_SIZE)
                    break;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            sender.close();
        } catch (IOException e) {
            System.err.println("Error stopping");
        }
    }

    public Part2Sender(String destAddress, int port, String filename) {
        serverPort = port;
        transferComplete = false;
        try {
            // Start file reading and create socket
            fileInputStream = new FileInputStream(filename);
            address = InetAddress.getByName(destAddress);
            socket = new DatagramSocket();
        } catch (IOException e) {
            System.out.println("IO exception!");
            e.printStackTrace();
        }
    }

    public void readFileData() throws IOException {
        packetData = new byte[PACKET_SIZE];
        bytesRead = fileInputStream.read(packetData, 0, PACKET_SIZE);
    }

    public void sendFileData() throws IOException {
        // Send data in chunks of PACKET_SIZE
        System.out.println(String.format("Read %d bytes", bytesRead));
        DatagramPacket packet = new DatagramPacket(packetData, bytesRead, address, serverPort);
        socket.send(packet);
        System.out.println("Sent packet");

        if (bytesRead != packetData.length)
            System.out.println("Packet length is short, so exiting");
    }

    public boolean receiveAck() throws IOException {
        // Receive ACK
        byte[] ackBytes = new byte["ACK".getBytes().length];
        DatagramPacket ackPacket = new DatagramPacket(ackBytes, ackBytes.length);
        socket.receive(ackPacket);
        if (byteArraysAreEqual(ackPacket.getData(), "ACK".getBytes())) {
            System.out.println("Received ACK");
            return true;
        }
        return false;
    }

    public void close() throws IOException {
        socket.close();
        fileInputStream.close();
    }

    public static boolean byteArraysAreEqual(byte[] a, byte[] b) {
        if (a.length != b.length)
            return false;
        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i])
                return false;
        }
        return true;
    }


    private static void arrayPrint(byte[] anByteArray) {
        for (byte Number: anByteArray) {
            System.out.format("%d \t", Number);
        }
    }
}
