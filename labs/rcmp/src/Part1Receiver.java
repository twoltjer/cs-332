import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class Part1Receiver {
    final int PACKET_SIZE = 1450;
    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("Program requires two arguments: a port number, and a file name to write to.");
            System.exit(1);
        }
        // Error in case argument is not a number
        int port = -1;
        try {
            port = Integer.parseInt(args[0]);
        } catch(NumberFormatException e) {
            System.err.println("Could not parse integer from port number argument. Exiting");
            System.exit(1);
        }
        // Set filename
        String filename = args[1];

        Part1Receiver receiver = new Part1Receiver(port, filename);
    }

    public Part1Receiver(int port, String filename) {
        try {
            // Create file output
            FileOutputStream fileOutputStream = new FileOutputStream(filename);

            // Start listening for packets
            byte[] packetData = new byte[PACKET_SIZE];
            DatagramSocket socket = new DatagramSocket(port);

            while (true)
            {
                // Receive a packet, and write it to the output file
                DatagramPacket packet = new DatagramPacket(packetData, PACKET_SIZE);
                socket.receive(packet);
                fileOutputStream.write(packet.getData(), 0, packet.getLength());
                System.out.println(String.format("Packet data length: %d bytes", packet.getLength()));
                if (packet.getLength() != PACKET_SIZE)
                    break;
            }

            // Clean up
            fileOutputStream.close();
            socket.close();
        } catch (SocketException e) {
            System.out.println("Socket exception!");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IO exception!");
            e.printStackTrace();
        }
    }
}
