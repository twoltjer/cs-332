import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.util.Random;

/**
 * Sends RCMP packets
 * By Thomas Woltjer and Charles Newton
 */
public class Part5Sender {
    private String filename;
    private RandomAccessFile fileInputStream;
    private InetAddress address;
    public int bytesRead;
    private DatagramSocket socket;
    private byte[] packetData;
    private int serverPort;
    private boolean transferComplete;
    public int packetNumber;
    private int connectionId;
    public int lastAckPacketNumber;
    public int ackGap;
    public int lastPacket;

    final int CONNECTION_ID_BYTES = 4;
    final int CONNECTION_ID_OFFSET = 0;
    final int FILE_SIZE_BYTES = 4;
    final int FILE_SIZE_OFFSET = CONNECTION_ID_OFFSET + CONNECTION_ID_BYTES;
    final int PACKET_NUMBER_BYTES = 4;
    final int PACKET_NUMBER_OFFSET = FILE_SIZE_OFFSET + FILE_SIZE_BYTES;
    final int PLEASE_ACK_BYTES = 1;
    final int PLEASE_ACK_OFFSET = PACKET_NUMBER_OFFSET + PACKET_NUMBER_BYTES;
    final int PAYLOAD_BYTES = 1450;
    final int PAYLOAD_OFFSET = PLEASE_ACK_OFFSET + PLEASE_ACK_BYTES;

    public static void main(String[] args) {
        if (args.length != 3) {
            System.err.println("Program requires three arguments: a host, a port number, and a file name to write to.");
            System.exit(1);
        }
        // Error in case argument is not a number
        int port = -1;
        try {
            port = Integer.parseInt(args[1]);
        } catch(NumberFormatException e) {
            System.err.println("Could not parse integer from port number argument. Exiting");
            System.exit(1);
        }
        // Set filename
        String filename = args[2];
        // Destination host
        String destHost = args[0];

        Part5Sender sender = new Part5Sender(destHost, port, filename);
        while (true) {
            try {
                sender.readFileData();
                sender.generateHeader();
                sender.sendFileData();
                if (sender.ackPacket()) {
                    try {
                        sender.receiveAck();
                        sender.lastAckPacketNumber = sender.packetNumber;
                        sender.ackGap++;
                    } catch (SocketTimeoutException e) {
                        // Rewind
                    }

                }
                if (sender.bytesRead < sender.PAYLOAD_BYTES)
                    break;
                sender.packetNumber++;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            sender.close();
        } catch (IOException e) {
            System.err.println("Error stopping");
        }
    }

    public Part5Sender(String destAddress, int port, String filename) {
        serverPort = port;
        transferComplete = false;
        this.filename = filename;
        this.packetNumber = 0;
        Random rand = new Random();
        this.connectionId = rand.nextInt();

        lastAckPacketNumber = 0;
        ackGap = -1;
        lastPacket = -1;

        try {
            // Start file reading and create socket
            fileInputStream = new RandomAccessFile(filename, "r");
            address = InetAddress.getByName(destAddress);
            socket = new DatagramSocket();
        } catch (IOException e) {
            System.out.println("IO exception!");
            e.printStackTrace();
        }
    }

    public void readFileData() throws IOException {
        packetData = new byte[PAYLOAD_OFFSET + PAYLOAD_BYTES];
        bytesRead = fileInputStream.read(packetData, PAYLOAD_OFFSET, PAYLOAD_BYTES);
        if (bytesRead < PAYLOAD_BYTES) {
            this.lastPacket = this.packetNumber;
        }
    }

    public void sendFileData() throws IOException {
        // Send data in chunks of PACKET_SIZE
        System.out.println(String.format("Read %d bytes", bytesRead));
        DatagramPacket packet = new DatagramPacket(packetData, PAYLOAD_OFFSET + bytesRead, address, serverPort);
        socket.send(packet);
        System.out.println("Sent packet");
        System.out.println("======================");
    }

    public void receiveAck() throws IOException, SocketTimeoutException {
        // Receive ACK
        byte[] ackBytes = new byte[CONNECTION_ID_BYTES + PACKET_NUMBER_BYTES];
        DatagramPacket ackPacket = new DatagramPacket(ackBytes, ackBytes.length);
        socket.setSoTimeout(1000); // 1 second timeout
        socket.receive(ackPacket);
        System.out.println("Received ACK");

        // Copy into byte arrays for each value
        byte[] connectionIdBytes = new byte[CONNECTION_ID_BYTES];
        for (int i = 0; i < CONNECTION_ID_BYTES; i++) {
            connectionIdBytes[i] = ackPacket.getData()[i];
        }
        int ackConnectionId = bytesToInt(connectionIdBytes);
        byte[] packetNumberBytes = new byte[PACKET_NUMBER_BYTES];
        for (int i = 0; i < PACKET_NUMBER_BYTES; i++) {
            packetNumberBytes[i] = ackPacket.getData()[i + CONNECTION_ID_BYTES];
        }
        int ackPacketNumber = bytesToInt(packetNumberBytes);
        System.out.println(String.format("Ack connection id: %d", ackConnectionId));
        System.out.println(String.format("Ack packet number: %d", ackPacketNumber));
        System.out.println("======================");
    }

    public boolean ackPacket() {
        if (this.lastAckPacketNumber + this.ackGap + 1 == this.packetNumber)
            return true;
        return (this.packetNumber == this.lastPacket);
    }

    public void close() throws IOException {
        socket.close();
        fileInputStream.close();
    }

    public static boolean byteArraysAreEqual(byte[] a, byte[] b) {
        if (a.length != b.length)
            return false;
        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i])
                return false;
        }
        return true;
    }

    public void generateHeader() {
        byte[] header = new byte[CONNECTION_ID_BYTES + FILE_SIZE_BYTES + PACKET_NUMBER_BYTES + PLEASE_ACK_BYTES];
        byte[] connectionId = getConnectionId();
        System.out.println(String.format("Connection ID: %d", this.connectionId));
        byte[] fileSize = getFileSize(filename);
        byte[] packetNumber = getPacketNumber();
        byte pleaseAck = (byte) (ackPacket() ? 1 : 0);
        System.out.println(String.format("Please ack: %d", pleaseAck));
        System.out.println(String.format("Packet #: %d", this.packetNumber));
        for (int i = 0; i < CONNECTION_ID_BYTES; i++) {
            header[i + CONNECTION_ID_OFFSET] = connectionId[i];
        }
        for (int i = 0; i < FILE_SIZE_BYTES; i++) {
            header[i + FILE_SIZE_OFFSET] = fileSize[i];
        }
        for (int i = 0; i < PACKET_NUMBER_BYTES; i++) {
            header[i + PACKET_NUMBER_OFFSET] = packetNumber[i];
        }
        header[PLEASE_ACK_OFFSET] = pleaseAck;
        for (int i = 0; i < header.length; i++)
            packetData[i] = header[i];
    }

    private byte[] getPacketNumber() {
        return intToBytes(this.packetNumber);
    }

    private byte[] getConnectionId() {
        return intToBytes(this.connectionId);
    }

    public byte[] getFileSize(String filename) {
        File f = new File(filename);
        return intToBytes((int) f.length());
    }

    public byte[] intToBytes(int value) {
        ByteBuffer buf = ByteBuffer.allocate(Integer.BYTES);
        buf.putInt(value);
        return buf.array();
    }

    public int bytesToInt(byte[] valueBytes) {
        ByteBuffer buf = ByteBuffer.allocate(Integer.BYTES);
        buf.put(valueBytes);
        return buf.getInt(0);
    }

    private static void arrayPrint(byte[] anByteArray) {
        for (byte Number: anByteArray) {
            System.out.format("%d \t", Number);
        }
    }
}
